spec:
  inputs:
    azure-cli-image:
      description: The image with Azure CLI preinstalled 
      default: mcr.microsoft.com/azure-cli
    azure-tenant-id:
      description: Tenant ID
      default: $AZURE_TENANT_ID
    azure-subscription-id:
      description: Subscription ID
      default: $AZURE_SUBSCRIPTION_ID
    azure-app-id:
      description: App ID
      default: $AZURE_APP_ID
    iac_scan_enabled:
      description: Add GitLab IAC Scanner
      default: 'true'
    azure-bicep-template-location:
      description: Relative path to Azure Bicep templates
      default: ./templates
    azure-bicep-template-main-file:
      description: Azure Bicep template file name
      default: main.bicep
    deployment-name:
      description: Azure Bicep deployment name
      default: $CI_PROJECT_NAME
    resource-group:
      description: Target resource group
      default: azure-bicep-gitlab
    
---

include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
    # TODO: add condition when this scan should run
    # rules:
    #   - if: '$IAC_SCAN_ENABLED == "true"'

variables:
  AZURE_CLI_IMAGE: $[[ inputs.azure-cli-image ]]
  AZURE_TENANT_ID: $AZURE_TENANT_ID
  AZURE_SUBSCRIPTION_ID: $AZURE_SUBSCRIPTION_ID
  AZURE_APP_ID: $AZURE_APP_ID
  # IAC_SCAN_ENABLED: $[[ inputs.iac_scan_enabled ]]
  AZURE_BICEP_TEMPLATE_LOCATION: $[[ inputs.azure-bicep-template-location ]]
  AZURE_BICEP_TEMPLATE_MAIN_FILE: $[[ inputs.azure-bicep-template-main-file ]]
  RESOURCE_GROUP: $[[ inputs.resource-group ]]
  DEPLOYMENT_NAME: $[[ inputs.deployment-name ]]
  FF_SCRIPT_SECTIONS: 1

.oidc_auth: &oidc_auth
  id_tokens:
    GITLAB_OIDC_TOKEN:
      aud: https://gitlab.com

default:
  image: 
    name: $AZURE_CLI_IMAGE

bicep2arm:
  script: az bicep build --file $AZURE_BICEP_TEMPLATE_LOCATION/$AZURE_BICEP_TEMPLATE_MAIN_FILE
  stage: build
  artifacts:
    paths:
      - $AZURE_BICEP_TEMPLATE_LOCATION/*.json

kics-iac-sast:
  dependencies: 
    - bicep2arm

lint:
  script: az bicep lint --file $AZURE_BICEP_TEMPLATE_LOCATION/$AZURE_BICEP_TEMPLATE_MAIN_FILE
  stage: test


what-if:
  <<: *oidc_auth
  script:
    - az login --service-principal -u $AZURE_APP_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN
    - az account set -s $AZURE_SUBSCRIPTION_ID
    - >
      az deployment group what-if \
        --mode Complete --name $DEPLOYMENT_NAME \
        --template-file $AZURE_BICEP_TEMPLATE_LOCATION/$AZURE_BICEP_TEMPLATE_MAIN_FILE \
        --resource-group $RESOURCE_GROUP
  stage: test

validate:
  <<: *oidc_auth
  script:
    - az login --service-principal -u $AZURE_APP_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN
    - az account set -s $AZURE_SUBSCRIPTION_ID
    - >
      az deployment group validate \
        --mode Complete --name $DEPLOYMENT_NAME \
        --template-file $AZURE_BICEP_TEMPLATE_LOCATION/$AZURE_BICEP_TEMPLATE_MAIN_FILE \
        --resource-group $RESOURCE_GROUP
  stage: test

deploy:
  <<: *oidc_auth
  script:
    - az login --service-principal -u $AZURE_APP_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN
    - az account set -s $AZURE_SUBSCRIPTION_ID
    - >
      az stack group create \
        --name $DEPLOYMENT_NAME \
        --template-file $AZURE_BICEP_TEMPLATE_LOCATION/$AZURE_BICEP_TEMPLATE_MAIN_FILE \
        --resource-group $RESOURCE_GROUP \
        --action-on-unmanage 'detachAll' \
        --deny-settings-mode 'none'
  stage: deploy

destroy:
  <<: *oidc_auth
  when: manual
  script:
    - az login --service-principal -u $AZURE_APP_ID -t $AZURE_TENANT_ID --federated-token $GITLAB_OIDC_TOKEN
    - az account set -s $AZURE_SUBSCRIPTION_ID
    - >
      az stack group delete \
        --name $DEPLOYMENT_NAME \
        --resource-group $RESOURCE_GROUP \
        --action-on-unmanage 'detachAll' --yes
  stage: destroy
