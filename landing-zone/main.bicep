param location string = 'westeurope'
param name string = uniqueString(resourceGroup().id)

resource storageaccount 'Microsoft.Storage/storageAccounts@2021-02-01' = {
  name: name
  location: location
  properties: { networkAcls: { defaultAction: 'Allow' } }
  kind: 'StorageV2'
  sku: {
    name: 'Premium_LRS'
  }
}
