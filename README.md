# GitLab CI/CD component for Azure Bicep

This CICD component implements a GitLab CI/CD template to build, test and deploy Cloud services in Azure using DSL [Azure Bicep](https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/gitlab-cicd-component/gitlab-cicd-component-azure-bicep@0.0.1
    inputs:
      azure-cli-image: mcr.microsoft.com/azure-cli
      azure-tenant-id: $AZURE_TENANT_ID
      azure-subscription-id: $AZURE_SUBSCRIPTION_ID
      azure-app-id: $AZURE_APP_ID
      azure-password: $AZURE_PASSWORD
      azure-bicep-template-location: ./landing-zone
      azure-bicep-template-main-file: main.bicep
      resource-group: azure-bicep-gitlab
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'gitlab-cicd-component/gitlab-cicd-component-azure-bicep'
    ref: '0.0.1'
    file: '/templates/azure-bicep.yml'

variables:
    AZURE_CLI_IMAGE: mcr.microsoft.com/azure-cli
    AZURE_BICEP_TEMPLATE_LOCATION: ./landing-zone
    AZURE_BICEP_TEMPLATE_MAIN_FILE: main.bicep
    RESOURCE_GROUP: azure-bicep-gitlab

```

## Service principal

Create [service principal](https://learn.microsoft.com/en-us/cli/azure/azure-cli-sp-tutorial-1?tabs=bash#create-a-service-principal).

The following [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) are expected:

- `AZURE_TENANT_ID`: Azure tenant ID
- `AZURE_SUBSCRIPTION_ID`: Azure Subscription ID
- `AZURE_APP_ID`: Service Principal Application ID
- `AZURE_PASSWORD`: Service Principal password

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `azure-cli-image` | `azure-cli-image` | The image with Azure CLI preinstalled |
| `azure-tenant-id` | `test` | Azure Tenant ID |
| `azure-subscription-id` | `test` | Azure Subscription ID |
| `azure-app-id` | `test` | App ID |
| `azure-password` | `test` | Password |
| `azure-bicep-template-location` | `./landing-zone` | Relative path to Azure Bicep templates |
| `azure-bicep-template-main-file` | `main.bicep` |Azure Bicep template file name |
| `resource-group` | `azure-bicep-gitlab` | Target resource group |

## OpenID Connect

Use [this documentation](https://docs.gitlab.com/ee/ci/cloud_services/azure/) to setup service principal and configure federated identity credentials. 

The following [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) are expected:

- `AZURE_TENANT_ID`: Azure tenant ID
- `AZURE_SUBSCRIPTION_ID`: Azure Subscription ID
- `AZURE_APP_ID`: Service Principal Application ID

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `azure-cli-image` | `azure-cli-image` | The image with Azure CLI preinstalled |
| `azure-tenant-id` | `test` | Azure Tenant ID |
| `azure-subscription-id` | `test` | Azure Subscription ID |
| `azure-app-id` | `test` | App ID |
| `azure-bicep-template-location` | `./landing-zone` | Relative path to Azure Bicep templates |
| `azure-bicep-template-main-file` | `main.bicep` |Azure Bicep template file name |
| `resource-group` | `azure-bicep-gitlab` | Target resource group |

Example of use: 

```yaml
include:
  - component: gitlab.com/gitlab-cicd-component/gitlab-cicd-component-azure-bicep-oidc@0.0.1
    inputs:
      azure-cli-image: mcr.microsoft.com/azure-cli
      azure-tenant-id: $AZURE_TENANT_ID
      azure-subscription-id: $AZURE_SUBSCRIPTION_ID
      azure-app-id: $AZURE_APP_ID
      azure-bicep-template-location: ./landing-zone
      azure-bicep-template-main-file: main.bicep
      resource-group: azure-bicep-gitlab
```

⚠️ Pay attention that the component name is different `gitlab.com/gitlab-cicd-component/gitlab-cicd-component-azure-bicep-oidc` (has `-oidc` at the end).

## Implementation

The CI/CD component caveats: 

- Uses [Deployment stacks](https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/deployment-stacks?tabs=azure-powershell) (Preview)
- Includes [Infrastructure as Code scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/)